import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MoviItemComponent } from './movi-item/movi-item.component';
import { MoviListComponent } from './movi-list/movi-list.component';
import { RatingMoviComponent } from './rating-movi/rating-movi.component';
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    MoviItemComponent,
    MoviListComponent,
    RatingMoviComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
