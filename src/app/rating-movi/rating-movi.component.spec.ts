import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingMoviComponent } from './rating-movi.component';

describe('RatingMoviComponent', () => {
  let component: RatingMoviComponent;
  let fixture: ComponentFixture<RatingMoviComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RatingMoviComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingMoviComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
