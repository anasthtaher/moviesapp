import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-rating-movi',
  templateUrl: './rating-movi.component.html',
  styleUrls: ['./rating-movi.component.css']
})
export class RatingMoviComponent implements OnInit {

  _rating: number;
  stars: number[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  @Input()
  set rating(rating: number){
    this._rating = rating;
    let j = Math.floor(this._rating);
    for(let i=0;i<j;i++){
      this.stars[i] = i;

    }
  }


}
