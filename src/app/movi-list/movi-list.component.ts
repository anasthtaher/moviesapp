import { Component, OnInit } from '@angular/core';
import { Movi } from '../movi';


@Component({
  selector: 'app-movi-list',
  templateUrl: './movi-list.component.html',
  styleUrls: ['./movi-list.component.css']
})
export class MoviListComponent implements OnInit {



  movies: Movi[] = [
    new Movi("Basketball Shoe", "2020", "Drama, Mystery", "https://i.pinimg.com/736x/78/c6/9e/78c69e6366ddb1c82797e179847129ca.jpg", 2.8),
    new Movi("Basketball Shoe", "1986", "Horror, SciFi", "https://i.pinimg.com/originals/ca/10/4f/ca104f8f69fac4da17b46c4a39772e48.jpg", 8.8),
    new Movi("Shawshank Redemption", "1994", "Drama", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT8KhiVcW0IR1LzxMaDstUoP-S9MAVRFCXbnCoCwF3B-eq_OKNeQmw_yMfZvnLcjHdOTb4&usqp=CAU", 6.6),
    new Movi("The Joker", "2019", "Drama, Horror", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT8KhiVcW0IR1LzxMaDstUoP-S9MAVRFCXbnCoCwF3B-eq_OKNeQmw_yMfZvnLcjHdOTb4&usqp=CAU", 2.6),
    new Movi("Basketball Shoe", "2019", "Drama, Horror", "https://i.pinimg.com/originals/b6/56/ea/b656ea095f4496cf8f1b023bed5e7705.jpg", 8.6),
    new Movi("The Joker", "2019", "Drama, Horror", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQsQ6C2XnuGUq1ALC6Gx2tivWiYpUhC_sVDCg1FjHoSGYtHJ7RUjU6Lskp9_Vyifn7q7_Y&usqp=CAU", 8),
    new Movi("The Joker", "2019", "Drama, Horror", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQsQ6C2XnuGUq1ALC6Gx2tivWiYpUhC_sVDCg1FjHoSGYtHJ7RUjU6Lskp9_Vyifn7q7_Y&usqp=CAU", 5.6),
    new Movi("Basketball Shoe", "2019", "Drama, Horror", "https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/7c702868473295.5b5ea9fa48430.jpg", 9.6),
    new Movi("The Joker", "2019", "Drama, Horror", "https://i.pinimg.com/originals/b6/56/ea/b656ea095f4496cf8f1b023bed5e7705.jpg", 5.6),
    new Movi("TBasketball Shoe", "2019", "Drama, Horror", "https://i.pinimg.com/originals/b6/56/ea/b656ea095f4496cf8f1b023bed5e7705.jpg", 3.6),
    new Movi("The Joker", "2019", "Drama, Horror", "https://i.pinimg.com/736x/78/c6/9e/78c69e6366ddb1c82797e179847129ca.jpg", 9.6),
    new Movi("The Joker", "2019", "Drama, Horror", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT8KhiVcW0IR1LzxMaDstUoP-S9MAVRFCXbnCoCwF3B-eq_OKNeQmw_yMfZvnLcjHdOTb4&usqp=CAU", 6.6),
    new Movi(" Joker", "2019", "Drama, Horror", "https://i.pinimg.com/originals/ca/10/4f/ca104f8f69fac4da17b46c4a39772e48.jpg", 2.6),
    new Movi("The Joker", "2019", "Drama, Horror", "https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/7c702868473295.5b5ea9fa48430.jpg", 7.6),
    new Movi("Basketball Shoe", "2019", "Drama, Horror", "https://i.pinimg.com/736x/78/c6/9e/78c69e6366ddb1c82797e179847129ca.jpg", 4.6),
    new Movi("Basketball Shoe", "2019", "Drama, Horror", "https://i.pinimg.com/originals/b6/56/ea/b656ea095f4496cf8f1b023bed5e7705.jpg", 5.6),

  ]

  constructor() { }

  ngOnInit(): void {
  }





  deleteMovieFromArray(movie: Movi){
    console.log("the movie to delete is " + movie.title);
      let index = this.movies.indexOf(movie);
      this.movies.splice(index, 1);
  }

}




