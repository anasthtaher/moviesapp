import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MoviListComponent } from './movi-list.component';

describe('MoviListComponent', () => {
  let component: MoviListComponent;
  let fixture: ComponentFixture<MoviListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MoviListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
