import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MoviItemComponent } from './movi-item.component';

describe('MoviItemComponent', () => {
  let component: MoviItemComponent;
  let fixture: ComponentFixture<MoviItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MoviItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
