import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Movi } from '../movi';

@Component({
  selector: 'app-movi-item',
  templateUrl: './movi-item.component.html',
  styleUrls: ['./movi-item.component.css']
})
export class MoviItemComponent implements OnInit {


  _movie: Movi;

  constructor() { }

  ngOnInit(): void {
  }

  @Input()
  set movie(movie: Movi){
    this._movie = movie;
  }

  @Output() deleteMovie = new EventEmitter<Movi>();

  delete(){
    this.deleteMovie.emit(this._movie);
  }



}
